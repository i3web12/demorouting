# Routing

## 1) Installer la librairie de routing :
```
    npm i react-router-dom
```

## 2) Indiquer à l'application qu'on veut utiliser un routing
Dans le fichier main.jsx, encapsuler le composant App par BrowserRouter de react-router-dom
```
    ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <BrowserRouter>    
        <App />
        </BrowserRouter>
    </React.StrictMode>,
    )
```

## 3) Créer ses pages
📁 pages<br/>
|------- Home.jsx<br/>
|------- Trainers.jsx<br/>
|------- TrainerDetails.jsx<br/>


## 4) Mettre en place notre fichier de routing


</br>
</br>
<hr>
</br>
</br>

# Json Server
https://www.npmjs.com/package/json-server
## Introduction 
C'est un outil qui permet de faire des fausses API (fakeAPI) avec du json en local sur votre machine. Très utile pour faire des tests ou travailler un front qui n'a pas encore de backend. Attention CE N'EST PAS UNE API !

## Mise en place
```
    npm i (-g) json-server  
```
(-g si on veut l'installer en global sur toute la machine)

Créer un fichier db.json et écrire :
```
    {
        "users" : [],
        "trainers" : [
            {  
                "id" : 1,
                "firstname" : "Aude",
                "lastname" : "Beurive",
                "birthdate": "1989-10-16",
                "hobbies" : ["Dessin", "Zumba", "Twitch"]
            }
        ],
        "courses" : []
    }
```
On obtient ainsi trois urls de base dispo
```
    localhost:3000/users
    localhost:3000/trainers
    localhost:3000/courses
```
Et les url avec l'id
```
    localhost:3000/trainers/2
```
Avec les méthodes GET, POST, PUT, PATCH et DELETE

## Utilisation
Pour lancer le "serveur" (en local) API
```
    npx json-server db.json
```
Attention : Vous devez le laisser tourner pour faire des requêtes dessus

### Pour ajouter des images sur le serveur
* Créer un dossier public et mettre vos images dedans
* Pour accèder à l'image -> http://localhost:3000/nomImage.ext

</br>
</br>
<hr>
</br>
</br>

# Navigation par ancres : 
https://www.npmjs.com/package/react-router-hash-link