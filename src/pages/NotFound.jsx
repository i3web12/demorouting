
const NotFound = () => {
    return (
        <>
            <h1>Oups... le contenu auquel vous essayez d'accéder n'existe pas...</h1>
        </>
    )
}

export default NotFound;