import axios from "axios";
import { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";

const TrainerDetails = () => {

    const [trainer, setTrainer] = useState({  id : 0, firstname : "", lastname : "", birthdate: "", hobbies : [] });

    //Hook qui permet de naviguer vers une autre page
    const navigate = useNavigate();

    //Hook de react-router-dom qui permet de récupérer un paramètre de la route (segment dynamique)
    const {id} = useParams();
    const url = "http://localhost:3000/trainers/";

    useEffect(() => {
        axios.get(url+id)
            .then((res) => {
                setTrainer(res.data)
            })
            .catch((err) => {
                if(err.response.status == 404) {
                    //Si erreur 404 de l'api, on va faire une redirection
                    navigate("/notfound");
                }
                
            })
    }, [])

    const removeTrainer = () => {
        axios.delete(url+id)
            .then(() => {
                navigate("/trainers");
            });
    }

    return (
        <>
            <h3>Détails du formateur n° { id }</h3>
            <NavLink to="/trainers" >Revenir à la liste des formateurs</NavLink>
            <div>
                <h4>{trainer.firstname} {trainer.lastname}</h4>
                <p>Né(e) le { new Date(trainer.birthdate).toLocaleDateString() }</p>
                <p>Ses hobbies sont :</p>
                <ul>
                    { trainer.hobbies.map((h, index) => (<li key={index} >{h}</li>)) }
                </ul>
                <button onClick={removeTrainer} >Supprimer le formateur</button>
            </div>
        </>
    )
}

export default TrainerDetails;