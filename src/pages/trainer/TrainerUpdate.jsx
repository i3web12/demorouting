import axios from "axios";
import { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom"

const TrainerUpdate = () => {
    const [person, setPerson] = useState({ firstname : "", lastname : "", birthdate : "", hobbies : [] })

    const {id} = useParams();

    const url = "http://localhost:3000/trainers/"

    const navigate = useNavigate();

    useEffect(() => {
        axios.get(url+id)
            .then((res) => {
                setPerson(res.data)
             })
            .catch((error) => {
                if(error.response.status == 404) {
                    navigate("/notfound");
                }
            })
    }, [])

    const changeValue = (e) => {
        const {id, value} = e.target;
        setPerson({
            ...person,
            [id] : value
        })
    }

    const updateTrainer = (e) => {
        e.preventDefault();

        if(person.firstname != "" && person.lastname != "" && person.birthdate != ""){
            axios.put(url+id, person)
                .then((res) => {
                    //Si notre insertion s'est bien passée :
                    navigate("/trainers");
                })
                .catch(() => {

                })
        }
    }

    return (
        <>
            <h3>Modifier un formateur</h3>
            <NavLink to="/trainers" >Revenir à la liste des formateurs</NavLink>

            <form onSubmit={updateTrainer} >
                <div>
                    <label htmlFor="lastname">Nom : </label>
                    <input id="lastname" type="text" placeholder="Nom" value={person.lastname} onChange={changeValue}/>
                </div>

                <div>
                    <label htmlFor="firstname">Prénom : </label>
                    <input id="firstname" type="text" placeholder="Prenom" value={person.firstname} onChange={changeValue} />
                </div>
                
                <div>
                    <label htmlFor="birthdate">Date de Naissance : </label>
                    <input id="birthdate" type="date" value={person.birthdate} onChange={changeValue} />
                </div>

                <div>
                    <input type="submit" value="Modifier" />

                </div>
            </form>
        </>
    )
}

export default TrainerUpdate;