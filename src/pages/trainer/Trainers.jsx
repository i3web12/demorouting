import axios from "axios";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

const Trainers = () => {
    
    const [trainers, setTrainers] = useState([]);
    const url = "http://localhost:3000/trainers/";

    useEffect(() => {

        axios.get(url)
            .then((res) => {
                setTrainers(res.data);
            })
            .catch(() => {})

    }, [])

    return (
        <>
            <h1>Découvrez nos formateurs :</h1>
            <ul>
                { trainers.map(t => 
                    (<li key={t.id}>
                        {t.firstname} {t.lastname} <NavLink to={'/trainers/'+t.id} >Détails</NavLink> <NavLink to={'/trainers/update/'+t.id} >Modifier</NavLink>
                      </li> )
                    )
                }
            </ul>
            <NavLink to="/trainers/add">Ajouter un formateur</NavLink>
        </>
    )
}

export default Trainers;