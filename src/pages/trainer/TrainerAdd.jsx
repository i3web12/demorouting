import axios from "axios";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom"

const TrainerAdd = () => {
    const [person, setPerson] = useState({ firstname : "", lastname : "", birthdate : "", hobbies : [] })
    const url = "http://localhost:3000/trainers/"

    const navigate = useNavigate();

    const changeValue = (e) => {
        const {id, value} = e.target;
        setPerson({
            ...person,
            [id] : value
        })
    }

    const addTrainer = (e) => {
        e.preventDefault();
        if(person.firstname != "" && person.lastname != "" && person.birthdate != ""){
            axios.post(url, person)
                .then((res) => {
                    //Si notre insertion s'est bien passée :
                    navigate("/trainers");
                })
                .catch(() => {

                })
        }
    }

    return (
        <>
            <h3>Ajouter un formateur</h3>
            <NavLink to="/trainers" >Revenir à la liste des formateurs</NavLink>

            <form onSubmit={addTrainer} >
                <div>
                    <label htmlFor="lastname">Nom : </label>
                    <input id="lastname" type="text" placeholder="Nom" value={person.lastname} onChange={changeValue}/>
                </div>

                <div>
                    <label htmlFor="firstname">Prénom : </label>
                    <input id="firstname" type="text" placeholder="Prenom" value={person.firstname} onChange={changeValue} />
                </div>
                
                <div>
                    <label htmlFor="birthdate">Date de Naissance : </label>
                    <input id="birthdate" type="date" value={person.birthdate} onChange={changeValue} />
                </div>

                <div>
                    <input type="submit" value="Ajouter" />

                </div>
            </form>
        </>
    )
}

export default TrainerAdd;