import { NavLink } from "react-router-dom"

const NavBar = () => {
  return (
    <header>
        <nav>
            <ul>
                <li>
                    <NavLink to="/" >Acceuil</NavLink>
                </li>
                <li>
                    <NavLink to="/trainers" >Nos Formateurs</NavLink>
                </li>
            </ul>
        </nav>
    </header>
  )
}

export default NavBar