import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { BrowserRouter, RouterProvider } from 'react-router-dom'
import router from './routes/routesV2.jsx'
import NavBar from './components/NavBar.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>    
    {/* Nouvelle façon depuis la dernière version : */}
    {/* <RouterProvider router={router} > */} 
      <App />
    {/* </RouterProvider> */}
    </BrowserRouter>
  </React.StrictMode>,
)
