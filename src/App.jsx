import { Route, Routes, useRoutes } from "react-router-dom"
import NavBar from "./components/NavBar"
import Home from "./pages/Home"
import Trainers from "./pages/trainer/Trainers"
import NotFound from "./pages/NotFound"

import routes from "./routes/routes" 

const App = () => {

  //Façon 2 : Grâce à la route useRoutes, on importe notre routing et on le met dans le rendu
  const appRoutes = useRoutes(routes)

  return (
    <>
      <NavBar />
      {appRoutes}
      {/* Une des façons de renseigner les routes : Directement dans le JSX */}
      {/* Inconvénient : Quand y'en a beaucoup, ça devient un peu */}
      {/* <Routes>
        <Route path="" element={ <Home /> } />
        <Route path="trainers" element={ <Trainers /> } /> */}
        {/* ↓ Toujours à la fin : Il signifie "si aucune des routes au dessus" */}
        {/* <Route path="*" element={ <NotFound />} />
      </Routes> */}
    </>
  )
}

export default App
