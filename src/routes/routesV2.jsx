import { createBrowserRouter } from "react-router-dom";
import Home from "../pages/Home";
import Trainers from "../pages/trainer/Trainers";
import NotFound from "../pages/NotFound";


const router = createBrowserRouter([
    {
        path : "",
        element : <Home />
    },
    {
        path : "trainers",
        element : <Trainers />
    },
    {
        path : "*",
        Element : <NotFound />
    }
])

export default router;