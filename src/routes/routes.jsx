
import { Navigate } from "react-router-dom";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";
import TrainerDetails from "../pages/trainer/TrainerDetails";
import Trainers from "../pages/trainer/Trainers";
import TrainerAdd from "../pages/trainer/TrainerAdd";
import TrainerUpdate from "../pages/trainer/TrainerUpdate";

const routes = [
    {
        path : "",
        element : (<Home />)
    },
    {
        path : "trainers",
        // element : <Trainers />,
        children : [
            { 
                path : "",
                element : <Trainers />
            },

            {
                // segment dynamique, id prendra une valeur
                path : ":id",
                element : <TrainerDetails />
            },

            { 
                path : "add",
                element : <TrainerAdd />
            },

            {
                path : "update/:id",
                element : <TrainerUpdate />
            }
        ]
    },
    {
        path : "notfound",
        element : <NotFound />
    },
    {
        path : "*",
        element : <Navigate to="/notfound" /> //Pour faire une redirection vers la route notfound au dessus
        
    }
]

export default routes;